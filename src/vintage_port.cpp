#include <cstring>
#include "vintage_port.h"

VintagePort::VintagePort(const char* br, const char* st, int b, const char* nn, int y)
    : Port(br, st, b)
    , nickname{new char[std::strlen(nn)]}
    , year(y) {
    strcpy(nickname, nn);
}

// Copy-constructor (const Port& p)
VintagePort::VintagePort(const VintagePort& vp) 
    : Port(vp)
    , nickname(vp.nickname)
    , year(vp.year) {}

VintagePort & VintagePort::operator= (const VintagePort & vp) {
    nickname = vp.nickname;
    year = vp.year;
    return *this;
}

void VintagePort::Show() const {
    std::cout << "Nickname: " << nickname << std::endl;
    std::cout << "Year: " << year << std::endl;
}

std::ostream& operator<< (std::ostream& os, const VintagePort& vp) {
    return os << vp.nickname << ", " << vp.year ;
}










