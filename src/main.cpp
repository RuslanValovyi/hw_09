#include "vintage_port.h"
#include <iostream>

int main() {
    Port *p1 = new VintagePort("Gallo", "tawny", 20, "The Noble", 1964);
    VintagePort *vp1 = new VintagePort("Kelto", "ruby", 34, "Old Velvet", 2001);
    VintagePort *vp2 = new VintagePort(*vp1);

    p1->Port::Show();
    p1->Show();
    std::cout << std::endl;

    vp2->Show();
    std::cout << std::endl;
    
    static_cast<Port>(*vp2).Show();
    std::cout << std::endl;
    
    std::cout << "p1: " << *p1 << std::endl;
    std::cout << std::endl;
    
    std::cout << "vp2: " << *vp2 << std::endl;
    std::cout << std::endl;
    
    delete vp1;
    delete vp2;
    delete p1;
    return 0;
}
