#include <cstring>
#include "port.h"

Port::Port(const char* br, const char* st, int b) 
    : brand{new char[std::strlen(br)]}
    , bottles(b) {
    strcpy(brand, br);
    strcpy(style, st);
}

// Copy-constructor (const Port& p)
Port::Port(const Port& p) 
    : brand(p.brand)
    , bottles(p.bottles) {
    strcpy(style, p.style);
}

Port& Port::operator= (const Port & p) {
    brand = p.brand;
    strcpy(style, p.style);
    bottles = p.bottles;
    return *this;
}

Port& Port::operator+= (int b) { // adds b to bottles
    bottles += b;
    return *this;
}

Port& Port::operator-= (int b) { // subtracts b from bottles, if possible
    if (bottles) {
        bottles -= b;
    }
    return *this;
}

void Port::Show() const {
    std::cout << "Brand: " << brand << std::endl;
    std::cout << "Kind: " << style << std::endl;
    std::cout << "Bottles: " << bottles << std::endl;
}

std::ostream& operator<< (std::ostream& os, const Port& p) {
    return os << p.brand << ", " << p.style << ", " << p.bottles;
}










